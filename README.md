# Slack Plugin for MSM

You need to have both the MSM Slack App installed on your organisation's Slack, as well as the Slack Plugin installed on your Service Desk.

## MSM-Slack Integration

This plugin allows you to publish a request's details to a specific workspace in Slack.

## Slack-MSM Integration

Within Slack you can associate chats with a published request (denoted by the channel's name) via the /associate command.

## Compatible Versions

| Plugin   | MSM     |
|----------|---------|
| 1.0.0    | 14.15.0 |
| 1.0.0.22 | 15.1+   |

## Installation

Install MSM Slack App on your Slack and configure it correctly.

Please see your MSM documentation for information on how to install plugins.

Once the plugin has been installed you will need to configure the following settings within the plugin page:

+ *Slack App OAuth Token*: The OAuth token for authentication with Slack App (found in the configured Slack App).
+ *Enable Mobile Link*: To activate mobile links, set the value to 1 otherwise by default it is deactivated.
+ *MSM API Key*: The MSM API�Key is a required field in order to send notes from Slack.
+ *Note Type*: Takes 1 or 2, where 1 is Public and 2 is Private. Notes will not be created from Slack if this field is not populated.

## Usage

The plugin can be launched from the quick menu after you load a request. On clicking the plugin you will be presented with a message "Would you like to publish this request to the Slack channel: INC-XXX?" Choosing No closes the popup with no further action. Choosing Yes creates a new Slack channel with the request type and number as the name e.g. "INC-XXX". The following request information is published to the newly created channel:

+ Request Type and Number with a description e.g. INC-1234 My Email is down.
+ Assignee information.
+ Status information.
+ Links to view the request in Self Service or Service Desk

## Slack Usage

The MSM Slack app will have a new command called /associate available. Any invalid date inputs have an appropriate error message. The slack preference's timezone and your local computer's timezone must match.

+ /associate - with no parameters adds all messages from within the current slack channel to the notes of the request associated with that channel.
+ /associate 2019.05.02.10:53:00 - with an oldest date parameter adds all messages from the current slack channel to the notes of the request associated with that channel.
+ /associate 2019.05.02.10:53:00 2019.05.02.11:00:30 - with an oldest and latest date parameters adds all messages from the current slack channel to the notes of the request associated with that channel.

## Contributing

We welcome all feedback including feature requests and bug reports. Please raise these as issues on GitHub. If you would like to contribute to the project please fork the repository and issue a pull request.
